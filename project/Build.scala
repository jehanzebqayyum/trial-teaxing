import sbt._
import Keys._

import play.Project._

object ApplicationBuild extends Build {

  val appName         = "teaXing"
  val appVersion      = "0.0.1-SNAPSHOT"

  val appDependencies = Seq(
    "org.reactivemongo" %% "play2-reactivemongo" % "0.10.2",
    "ws.securesocial" %% "securesocial" % "2.1.3"
  )

  val sonatype = "Sonatype Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"

  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += sonatype
    )

}
